module bitbucket.org/alima123/requester

go 1.13

require (
	bitbucket.org/alima123/expires v0.0.0-20191226173914-cddaf5cffb3d
	bitbucket.org/alima123/rio v0.0.0-20191226174050-1672072b40ae
)
